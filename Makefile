## Makefile downloads from git repository and extracts ttf/otf files. Use
## createdeb.sh to build debian package.

ttfsrc=https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/NerdFontsSymbolsOnly.tar.xz
ttfout=$(shell basename ${ttfsrc})

all:
	test -f ${ttfout} || wget ${ttfsrc}
	tar xvf ${ttfout} --one-top-level=ttf 

clean:
	rm -f ${ttfout} 
	rm -rf ttf/ 
